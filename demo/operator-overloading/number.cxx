#include <iostream>
#include <iomanip>
#include <variant>
#include <utility>

namespace ribomation {
    using namespace std;

    class Number {
        variant<long, double> value = 0L;

    public:
        Number(long v) : value{v} {}
        Number(double v) : value{v} {}
        Number(int v) : Number{static_cast<long>(v)} {}
        Number(float v) : Number{static_cast<double>(v)} {}

        auto isIntegral() const -> bool {
            return holds_alternative<long>(value);
        }
        auto isReal() const -> bool {
            return holds_alternative<double>(value);
        }

        auto integral() const -> long {
            if (auto v = get_if<long>(&value); v != nullptr) return *v;
            return static_cast<long>(get<double>(value));
        }
        auto real() const -> double {
            if (auto v = get_if<double>(&value); v != nullptr) return *v;
            return static_cast<double>(get<long>(value));
        }

        friend auto operator <<(ostream& os, const Number& n) -> ostream& {
            return os << n.real();
        }
    };

    auto operator -(const Number n) -> Number {
        if (n.isIntegral()) return {-n.integral()};
        return {-n.real()};
    }

    auto operator !(const Number n) -> Number {
        if (n.isIntegral()) return {1L / n.integral()};
        return {1.0 / n.real()};
    }

    auto operator +(const Number lhs, const Number rhs) -> Number {
        if (lhs.isIntegral() && rhs.isIntegral()) {
            return {lhs.integral() + rhs.integral()};
        }
        return {lhs.real() + rhs.real()};
    }

    auto operator *(const Number lhs, const Number rhs) -> Number {
        if (lhs.isIntegral() && rhs.isIntegral()) {
            return {lhs.integral() * rhs.integral()};
        }
        return {lhs.real() * rhs.real()};
    }

    auto operator ++(Number& n) -> Number {
        if (n.isIntegral()) { n = n.integral() + 1;
        } else { n = n.real() + 1;
        }
        return n;
    }

    auto operator ==(const Number lhs, const Number rhs) -> bool {
        if (lhs.isIntegral() && rhs.isIntegral()) {
            return lhs.integral() == rhs.integral();
        }
        auto const tolerance = double{0.0001};
        return abs(lhs.real() - rhs.real()) < tolerance;
    }

    auto operator <(const Number lhs, const Number rhs) -> bool {
        if (lhs.isIntegral() && rhs.isIntegral()) {
            return lhs.integral() < rhs.integral();
        }
        return lhs.real() < rhs.real();
    }

    auto operator -(const Number lhs, const Number rhs) -> Number { return lhs + -rhs; }
    auto operator /(const Number lhs, const Number rhs) -> Number { return lhs * !rhs; }
    auto operator ++(Number& n, int) -> Number { auto old = n; ++n; return old; }
}


int main() {
    using namespace std;
    using namespace std::rel_ops;
    using namespace ribomation;
    cout << boolalpha << setprecision(4);

    auto a = Number{42};
    cout << "a: " << a << " (integral==" << a.isIntegral() << ")" << endl;

    auto b = Number{3.5F};
    cout << "b: " << b << " (real==" << b.isReal() << ")" << endl;

    cout << "a + b : " << a + b << endl;
    cout << "a - b : " << a - b << endl;
    cout << "a * b : " << a * b << endl;
    cout << "a * 10: " << a * 10 << endl;
    cout << "a / b : " << a / b << endl;

    cout << "a == b: " << (a == b) << endl;
    cout << "b == b: " << (b == b) << endl;
    cout << "b == 3.50005: " << (b == 3.50005) << endl;
    cout << "a != b: " << (a != b) << endl;
    cout << "a <  b: " << (a < b) << endl;
    cout << "a <= b: " << (a <= b) << endl;
    cout << "a >  b: " << (a > b) << endl;
    cout << "a >= b: " << (a >= b) << endl;

    cout << "++a: " << ++a << endl;
    cout << "a++: " << a++ << " --> " << a << endl;
    cout << "++b: " << ++b << endl;
    cout << "b++: " << b++ << " --> " << b << endl;

    return 0;
}
