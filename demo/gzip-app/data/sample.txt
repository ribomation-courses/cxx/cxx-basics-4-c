Problem:
You’ve got a gzipped file that you want to decompress using C++.
You don’t want to use pipes to gzip in an external process.
You don’t want to use zlib and manual buffering either.

Solution:
This is an extension of the official gzip_decompressor example.
It is also applicable to bzip2 files, assuming you use the correct decompressor filter.
The program takes a single command line argument (which is a gzipped file) and
prints its decompressed output to stdout.
