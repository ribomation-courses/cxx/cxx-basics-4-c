#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <filesystem>
#include <unordered_map>
#include <vector>
#include <algorithm>
#include <utility>
#include <chrono>
#include <locale>

#include <cerrno>
#include <cstring>
#include <cctype>

#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/filter/gzip.hpp>

using namespace std;
using namespace std::literals;
namespace fs = std::filesystem;
namespace bio = boost::iostreams;
using Freqs = unordered_map<string, unsigned long>;
using WordFreq = pair<string, unsigned long>;

auto strip(string s) -> string {
    auto result = s;
    transform(begin(s), end(s), begin(result), [](auto ch) {
        return ::isalpha(ch) ? ::tolower(ch) : ' ';
    });
    return result;
}

auto load(istream& in, unsigned minWordSize, unsigned& lineCnt) -> Freqs {
    auto        freqs = Freqs{};
    for (string line; getline(in, line);) {
        ++lineCnt;
        auto        buf = istringstream{strip(line)};
        for (string word; buf >> word;) {
            if (word.size() >= minWordSize) { ++freqs[word]; }
        }
    }
    return freqs;
}

auto mostFrequentWords(Freqs freqs, unsigned maxWordCount) -> vector<WordFreq> {
    auto wordFreqs     = vector<WordFreq>{maxWordCount};
    auto descFreqOrder = [](const WordFreq& lhs, const WordFreq& rhs) { return lhs.second > rhs.second; };
    partial_sort_copy(
            begin(freqs), end(freqs),
            begin(wordFreqs), end(wordFreqs),
            descFreqOrder);
    return wordFreqs;
}

auto longestWord(vector<WordFreq> wordFreqs) -> unsigned {
    auto ascWordSizeOrder = [](WordFreq lhs, WordFreq rhs) { return lhs.first.size() < rhs.first.size(); };
    return max_element(begin(wordFreqs), end(wordFreqs), ascWordSizeOrder)->first.size();
}

int main(int argc, char** argv) {
    cout.imbue(locale{"en_US.UTF8"});

    auto dir          = fs::path("/mnt/c/Users/jensr/Dropbox/Ribomation/Ribomation-Training-2017-Autumn/large-files"s);
    auto filename     = dir / "english.100MB.gz"s;
    auto minWordSize  = 4U;
    auto maxWordCount = 100U;

    auto file = ifstream{filename, ios_base::in | ios_base::binary};
    if (!file) throw invalid_argument{"cannot open "s + filename.string() + ": "s + strerror(errno)};

    auto zin = bio::filtering_streambuf<bio::input>{};
    zin.push(bio::gzip_decompressor());
    zin.push(file);
    auto in = istream{&zin};

    cout << "Processing file " << filename << " ...\n";
    auto startTime = chrono::steady_clock::now();
    auto lineCnt   = 1U;
    auto freqs     = load(in, minWordSize, lineCnt);
    auto wordFreqs = mostFrequentWords(freqs, maxWordCount);
    auto maxSize   = longestWord(wordFreqs);
    auto endTime = chrono::steady_clock::now();

    for (auto const&[word, freq] : wordFreqs) {
        cout << setw(maxSize) << left << word << ": " << setw(8) << right << freq << endl;
    }

    cout << "----------------\n";
    cout << "# lines: " << lineCnt << endl;
    cout << "# words: " << freqs.size() << endl;
    cout << "elapsed: " << chrono::duration_cast<chrono::seconds>(endTime - startTime).count() << " secs" << endl;

    return 0;
}
