#include <iostream>
#include <string>
#include <stdexcept>

#include <cerrno>
#include <cstring>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

using namespace std;
using namespace std::literals;

int main(int argc, char** argv) {
    auto filename = "../file-meta-data.cxx"s;

    struct stat info{};
    if (stat(filename.c_str(), &info) == -1) {
        throw invalid_argument{"cannot open "s + filename + ": "s + strerror(errno)};
    }
    cout << "size: " << info.st_size << " bytes\n";

    return 0;
}
