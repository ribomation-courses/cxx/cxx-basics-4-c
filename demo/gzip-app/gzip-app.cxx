#include <iostream>
#include <fstream>
#include <string>
#include <filesystem>

#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/filter/gzip.hpp>

using namespace std;
using namespace std::literals;
namespace fs = std::filesystem;
namespace bio = boost::iostreams;

int main(int argc, char** argv) {
    auto filename = fs::path{"../data/sample.txt.gz"s};

    auto file = ifstream{filename, ios_base::in | ios_base::binary};
    if (!file) throw invalid_argument{"cannot open "s + filename.string() + ": "s + strerror(errno)};

    auto zin = bio::filtering_streambuf<bio::input>{};
    zin.push(bio::gzip_decompressor());
    zin.push(file);
    auto in = istream{&zin};

    auto lineno = 1U;
    for (string line; getline(in, line);) {
        cout << lineno++ << ") " << line << endl;
    }

    return 0;
}
