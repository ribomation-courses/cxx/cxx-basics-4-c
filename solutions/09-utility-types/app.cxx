#include <iostream>
#include "contact-card.hxx"

#include <bitset>
#include <iomanip>

using namespace std;
using namespace std::literals;
using namespace ribomation;

int main() {
    ContactCard cc{"Nisse"};

    cout << "CC: " << cc << endl;
    cout << "CC: " << cc.email("nisse@gmail.com"s) << endl;
    cout << "CC: " << cc.age(42).address("17 Reboot Lane"s) << endl;
    cout << "CC: " << cc.reset().age(57) << endl;

    auto value = 0xf003;
    cout << "value: " << bitset<64>(value) << endl;
    cout << "value: 0x" << hex << value << endl;
    cout << "value: 0x" << setw(8)<<setfill('0') << hex << value << endl;

    return 0;
}
