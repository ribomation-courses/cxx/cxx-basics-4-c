#pragma once
#include <string>
#include <optional>
#include <iosfwd>

namespace ribomation {
    using namespace std;
    using namespace std::literals;

    struct ContactCard {
        string const       name_;
        optional<string>   email_;
        optional<unsigned> age_;
        optional<string>   address_;

        explicit ContactCard(string name) : name_(std::move(name)) {}

        ContactCard& email(string s) {
            email_ = s;
            return *this;
        }

        ContactCard& age(unsigned s) {
            age_ = s;
            return *this;
        }

        ContactCard& address(string s) {
            address_ = s;
            return *this;
        }

        ContactCard& reset() {
            email_.reset();
            age_.reset();
            address_.reset();
            return *this;
        }

        string name() const {
            return name_;
        }

        string email() const {
            return email_.value_or("");
        }

        unsigned age() const {
            return age_.value_or(0);
        }

        string address() const {
            return address_.value_or("");
        }

        friend ostream& operator <<(ostream& os, const ContactCard& cc) {
            os << "Contact{";
            os << "name: " << cc.name();
            if (cc.age_.has_value()) os << ", age: " << cc.age();
            if (cc.email_.operator bool()) os << ", email: " << cc.email();
            if (cc.address_) os << ", address: " << cc.address();
            os << "}";
            return os;
        }
    };

}
