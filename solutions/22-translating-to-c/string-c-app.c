#include <stdio.h>
#include <stdlib.h>

extern const char* _Z11toUpperCasePKc(const char*);
//$ nm ./cmake-build-debug/CMakeFiles/string-app.dir/string/string-util.cxx.o | grep -i upper
//0000000000000000 T _Z11toUpperCasePKc

int main(int argc, char** argv) {
    const char* src = "tjabba habba babba";
    const char* dst = _Z11toUpperCasePKc(src);
    
    printf("[C] %s --> %s\n", src, dst);
    
    free((void*)dst);
    
    return 0;
}
