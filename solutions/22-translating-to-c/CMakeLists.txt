cmake_minimum_required(VERSION 3.14)
project(22_translating_to_c)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_C_STANDARD 99)

add_executable(str-cxx
        string-util.hxx
        string-util.cxx
        string-util-app.cxx
        )

add_executable(str-c
        string-util.hxx
        string-util.cxx
        string-c-app.c
        )

