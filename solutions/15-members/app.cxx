#include <iostream>
#include <vector>
#include "person.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;

int Dog::instanceCount    = 0;
int Person::instanceCount = 0;

auto func(Person q) -> Person {
    q.incrAge();
    cout << "[func] q: " << q.toString() << endl;
    return q;
}

int main() {
    cout << "[main] enter\n";

    {
        auto p = Person{"Per Silja"s, 42};
        cout << "1 p: " << p.toString() << endl;
        {
            auto d = Dog{"Fido"};
            p.setDog(&d);
            cout << "2 p: " << p.toString() << endl;
        }
        {
            auto d = Dog{"Caro"};
            cout << "d: " << d.toString() << endl;
        }
        cout << "4 p: " << p.toString() << " -- Ooops!!" << endl;
        {
            auto x = func(p);
            cout << "3 x: " << x.toString() << endl;
        }
        cout << "5 p: " << p.toString() << " -- Ooops!!" << endl;
    }

    cout << "[main] Dog   ::instanceCount: " << Dog::getInstanceCount() << endl;
    cout << "[main] Person::instanceCount: " << Person::getInstanceCount() << endl;
    cout << "[main] exit\n";
    return 0;
}
