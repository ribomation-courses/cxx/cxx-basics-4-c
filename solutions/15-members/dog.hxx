#pragma once

#include <iostream>
#include <sstream>
#include <string>

namespace ribomation {
    using namespace std;
    using namespace std::literals;

    class Dog {
        const string name;
        static int   instanceCount;

    public:
        Dog(string n) : name{n} {
            ++instanceCount;
            cout << "Dog(" << name << ") @ " << this << " [" << instanceCount << "] " << endl;
        }

        ~Dog() {
            --instanceCount;
            cout << "~Dog() @ " << this << " [" << instanceCount << "] " << endl;
        }

        void bark() const {
            cout << "Dog " << name << " says voff, voff\n";
        }

        string toString() const {
            ostringstream buf{};
            buf << "Dog(" << name << ") @ " << this << " [" << instanceCount << "]";
            return buf.str();
        }

        static int getInstanceCount() {
            return instanceCount;
        }
    };

}
