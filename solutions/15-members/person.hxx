#pragma once

#include <string>
#include <sstream>
#include "dog.hxx"

namespace ribomation {
    using namespace std;
    using namespace std::literals;

    class Person {
        const string name;
        unsigned     age = 0;
        Dog* dog = nullptr;
        static int instanceCount;

    public:
        Person(string n, unsigned a)
                : name{n}, age{a} {
            ++instanceCount;
            cout << "+Person(" << name << ", " << age << " @ " << " [" << instanceCount << "] " << this << endl;
        }

        Person(const Person& that): name{that.name}, age{that.age}{
            ++instanceCount;
            cout << "+Person(const Person& " << name << ", " << age << " @ " << " [" << instanceCount << "] " << this << endl;
        }

        ~Person() {
            --instanceCount;
            cout << "~Person()" << " @ " << " [" << instanceCount << "] " << this << endl;
        }

        string toString() const {
            ostringstream buf{};
            buf << "Person(" << name << ", " << age
                << (dog == nullptr ? "" : ", "s + dog->toString())
                << ") @ " << this << " [" << instanceCount << "]";
            return buf.str();
        }

        void setDog(Dog* d) {
            dog = d;
        }

        void incrAge() {
            ++age;
        }

        static int getInstanceCount() {
            return instanceCount;
        }
    };
}
