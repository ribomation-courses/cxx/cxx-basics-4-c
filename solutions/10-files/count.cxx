#include <iostream>
#include <sstream>
#include <string>
#include <tuple>
#include "each-line.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;

auto count(const string& filename) -> tuple<unsigned, unsigned, unsigned>;
auto countWords(const string& line) -> unsigned;

int main(int argc, char** argv) {
    auto       lineCnt = 0U;
    auto       wordCnt = 0U;
    auto       charCnt = 0U;
    auto const TAB     = "\t"s;
    auto       total   = "total"s;

    if (argc > 1) {
        for (auto k = 1U; k < argc; ++k) {
            auto filename = string{argv[k]};
            auto [lines, words, chars] = count(filename);
            cout << lines << TAB << words << TAB << chars << TAB << filename << endl;
            lineCnt += lines;
            wordCnt += words;
            charCnt += chars;
        }
    } else {
        eachLine(cin, [&](auto line) {
            ++lineCnt;
            wordCnt += countWords(line);
            charCnt += line.size() + 1;
        });
        total = "stdin"s;
    }
    if (argc != 2) {
        cout << lineCnt << TAB << wordCnt << TAB << charCnt << TAB << total << endl;
    }

    return 0;
}

auto count(const string& filename) -> tuple<unsigned, unsigned, unsigned> {
    auto lineCnt = 0U;
    auto wordCnt = 0U;
    auto charCnt = 0U;

    eachLine(filename, [&](auto line) {
        ++lineCnt;
        wordCnt += countWords(line);
        charCnt += line.size() + 1;
    });

    return {lineCnt, wordCnt, charCnt};
}

auto countWords(const string& line) -> unsigned {
    auto        wordCnt = 0U;
    auto        buf     = istringstream{line};
    for (string word; buf >> word;) ++wordCnt;
    return wordCnt;
}
