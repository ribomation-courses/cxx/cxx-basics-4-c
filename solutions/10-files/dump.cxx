#include <iostream>
#include <iomanip>
#include "each-line.hxx"
using namespace std;
using namespace std::literals;
using namespace ribomation;

int main() {
    auto lineno = 1U;

    eachLine("../dump.cxx"s, [&lineno](auto line) {
        cout << setw(3) << setfill('0') << lineno++ << ": " << line << endl;
    });

    return 0;
}
