#pragma once

#include <fstream>
#include <sstream>
#include <functional>
#include <stdexcept>
#include <cstring>
#include <cerrno>

namespace ribomation {
    using namespace std;
    using namespace std::literals;

    inline void eachLine(istream& is, function<void(string)> handle) {
        for (string line; getline(is, line);) {
            handle(line);
        }
    }

    inline void eachLine(string filename, function<void(string)> handle) {
        auto file = ifstream{filename};
        if (!file) throw invalid_argument{"cannot open "s + filename + ": "s + strerror(errno)};

        eachLine(file, handle);
    }
}
