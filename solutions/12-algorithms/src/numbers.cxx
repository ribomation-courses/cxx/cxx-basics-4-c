#include <iostream>
#include <iterator>
#include <vector>
#include <algorithm>
#include <numeric>
#include <cmath>
using namespace std;

int main() {
    auto in      = istream_iterator<double>{cin};
    auto eof     = istream_iterator<double>{};
    auto numbers = vector<double>{in, eof};

    auto [min, max] = minmax_element(begin(numbers), end(numbers));
    cout << "boundary: [" << *min << ", " << *max << "]" << endl;

    auto mean = accumulate(begin(numbers), end(numbers), 0.0) / size(numbers);
    cout << "mean: " << mean << endl;

    auto sq     = [](auto x) { return x * x; };
    auto stddev = sqrt(accumulate(begin(numbers), end(numbers), 0.0,
                                  [mean, sq](auto sum, auto x) { return sum + sq(x - mean); })
                       / size(numbers));
    cout << "stddev: " << stddev << endl;

    return 0;
}

