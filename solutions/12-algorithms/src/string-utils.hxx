#pragma once
#include <string>
#include <cctype>
#include <algorithm>
#include <iterator>

namespace ribomation {
    using namespace std;
    using namespace std::literals;

    inline string strip(const string& s) {
        auto result = ""s;
        transform(s.begin(), s.end(), back_inserter(result), [](char ch){
            return ::isalpha(ch) ? ch : ' ';
        });
        return result;
    }

}
