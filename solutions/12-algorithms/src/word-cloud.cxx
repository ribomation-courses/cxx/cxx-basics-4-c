#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>
#include <algorithm>
#include <stdexcept>
#include <random>
#include <cstdio>

#include "each-line.hxx"
#include "string-utils.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;

using WordFreqs = unordered_map<string, unsigned>;
using WordFreq = pair<string, unsigned>;

auto load(const string& filename) -> WordFreqs;
auto dump(const WordFreqs&) -> void;

auto sortFreqs(const WordFreqs&, unsigned) -> vector<WordFreq>;
auto dump(const vector<WordFreq>&) -> void;

auto mkTags(const vector<WordFreq>&) -> vector<string>;
auto dump(const vector<string>&) -> void;

auto store(string, const vector<string>&) -> void;

int main(int argc, char** argv) {
    auto filename = (argc == 1) ? "../data/shakespeare.txt"s : argv[1];
    auto N        = 100;

    auto wordFreqs = load(filename);
    //dump(wordFreqs);

    auto mostFreqs = sortFreqs(wordFreqs, N);
    //dump(mostFreqs);

    auto tags = mkTags(mostFreqs);
    //dump(tags);

    random_device r;
    shuffle(tags.begin(), tags.end(), r);

    auto htmlfile = "../html/word-cloud.html"s;
    store(htmlfile, tags);
    cout << "written " << htmlfile << endl;

    return 0;
}


auto load(const string& filename) -> WordFreqs {
    auto freqs = WordFreqs{};

    eachLine(filename, [&freqs](auto line) {
        auto stripped = strip(line);

        auto        buf = istringstream{stripped};
        for (string word; buf >> word;) {
            if (word.size() > 3) {
                freqs[word]++;  //make_pair(word, unsigned{})
            }
        }
    });

    return freqs;
}

auto dump(const WordFreqs& freqs) -> void {
    for (auto const &[word, freq] : freqs) {
        cout << word << ":" << freq << endl;
    }
}

auto sortFreqs(const WordFreqs& freqs, unsigned N) -> vector<WordFreq> {
    auto sortable = vector<WordFreq>{freqs.begin(), freqs.end()};

    auto byFreqsDesc = [](auto lhs, auto rhs) {
        return lhs.second > rhs.second;
    };
    sort(sortable.begin(), sortable.end(), byFreqsDesc);

    auto mostFreq = vector<WordFreq>{};
    copy_n(sortable.begin(), N, back_inserter(mostFreq));

    return mostFreq;
}

auto dump(const vector<WordFreq>& freqs) -> void {
    for (auto const &[word, freq] : freqs) {
        cout << word << ":" << freq << endl;
    }
}

auto mkTags(const vector<WordFreq>& freqs) -> vector<string> {
    auto const minFont = 10U;
    auto const maxFont = 100U;
    auto const maxFreq = freqs.front().second;
    auto const minFreq = freqs.back().second;
    auto const scale   = static_cast<double>(maxFont - minFont) / (maxFreq - minFreq);

    auto tags = vector<string>{};
    for (auto const &[word, freq] : freqs) {

        auto size = static_cast<unsigned>(freq * scale) + minFont;
        char buf[1024];
        sprintf(buf, "<span style=\"font-size: %dpx;\">%s</span>", size, word.c_str());
        tags.push_back(string{buf});
    }

    return tags;
}

auto dump(const vector<string>& tags) -> void {
    for (auto const& tag : tags) {
        cout << tag << endl;
    }
}

auto store(string filename, const vector<string>& tags) -> void {
    auto file = ofstream{filename};
    if (!file) throw invalid_argument{"cannot open "s + filename};

    auto prefix = R"(
<html>
<head>
    <title>Word Cloud</title>
</head>
<body>
    <h1>Word Cloud</h1>
)";
    auto suffix = "</body>\n</html>"s;

    file << prefix << endl;
    for (auto const& tag  : tags) {
        file << tag << endl;
    }
    file << suffix << endl;
}

