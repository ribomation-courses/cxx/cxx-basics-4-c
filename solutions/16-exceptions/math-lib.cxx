#include <sstream>
#include <cmath>
#include "math-lib.hxx"

namespace ribomation {
    using namespace std::string_literals;
    using std::ostringstream;

    auto log_2(double x) -> double {
        auto result = std::log2(x);
        if (std::isfinite(result)) {
            return result;
        }

        if (std::isnan(result)) {
            ostringstream buf{};
            buf << "log2("<<x<<") --> NaN";
            throw MathError{buf.str()};
        }

        if (std::isinf(result)) {
            ostringstream buf{};
            buf << "log2("<<x<<") --> oo";
            throw MathError{buf.str()};
        }

        throw MathError{"log2: unknown error"s};
    }

}
