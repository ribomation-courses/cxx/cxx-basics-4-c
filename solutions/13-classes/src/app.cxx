#include <iostream>
#include <vector>
#include "person.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;

int main() {
    cout << "[main] enter\n";

/*
    {
        auto p = Person{"Per Silja"s, 42};
        cout << "p: " << p.toString() << endl;
        p.incrAge();
        cout << "p: " << p.toString() << endl;
    }
*/

    cout << "[main] -------------\n";
    {
        auto persons = vector<Person>{
                {"Per Silja"s, 42},
                {"Justin Time"s, 32},
                {"Anna Conda"s, 22},
                {"Inge Vidare"s, 52},
                {"Sham Poo"s, 62},
        };
        for (auto const& p : persons)
            cout << "p: " << p.toString() << endl;
    }
    cout << "[main] exit\n";
    return 0;
}
