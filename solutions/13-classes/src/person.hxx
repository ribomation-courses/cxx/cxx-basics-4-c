#pragma once
#include <iostream>
#include <sstream>
#include <string>

namespace ribomation {
    using namespace std;
    using namespace std::literals;

    class Person {
        const string name;
        unsigned          age = 0;

    public:
        Person(string n, unsigned a = 42) : name{n}, age{a} {
            cout << "+Person{" << name << ", " << age << "} @ " << this << endl;
        }

        ~Person() {
            cout << "~Person" << " @ " << this << endl;
        }

        string toString() const {
            auto buf = ostringstream{};
            buf << "Person{" << name << ", " << age << "} @ " << this;
            return buf.str();
        }

        void incrAge() {
            ++age;
        }
    };

}
