#include <iostream>
using namespace std;

void multiply(int a, int& b) {
    cout << "multiply(int " << a << ", int& "<< b << ")\n";
    b = a * 42;
}

void multiply(int a, int&& b) {
    cout << "multiply(int " << a << ", int&& "<< b << ")\n";
    b = a * 42;
}

int main() {
    int result = 1;
    cout << "result: " << result << endl;

    multiply(1, result);
    cout << "result: " << result << endl;

    multiply(2, result);
    cout << "result: " << result << endl;

    multiply(3, result * 2 + 10);
    cout << "result: " << result << endl;
    return 0;
}
