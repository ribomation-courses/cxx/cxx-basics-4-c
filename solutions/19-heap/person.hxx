#pragma once

#include <iostream>
#include <string>

namespace ribomation {
    using namespace std;

    class Person {
        const string name;
        unsigned     age = 0;
        static int   instanceCount;

        static auto dump(string prefix, ostream& os, const Person& p) -> ostream& {
#ifndef NDEBUG
            return os << prefix << "("
                      << (p.name.empty() ? "*" : p.name)
                      << ", " << p.age
                      << ") @ " << &p
                      << " [#" << instanceCount << "]"
                      << endl;
#else
            return os;
#endif
        }

    public:
        static int getCount() { return instanceCount; }

        ~Person() {
            --instanceCount;
            dump("~Person", cout, *this) ;
        }

        Person() {
            ++instanceCount;
            dump("Person", cout, *this) ;
        }

        Person(string n, unsigned a) : name{n}, age{a} {
            ++instanceCount;
            dump("Person<string,int>", cout, *this) ;
        }

        Person(const Person& that) : name{that.name}, age{that.age} {
            ++instanceCount;
            dump("Person<const Person&>", cout, *this) ;
        }

        friend auto operator <<(ostream& os, const Person& p) -> ostream& {
            return dump("Person", os, p);
        }
    };

}
