#include <iostream>
#include <vector>
#include "person.hxx"
using namespace std;
using namespace std::literals;
using namespace ribomation;

int main() {
    {
        auto p1 = Person{};
        auto p2 = Person{"anna", 42};
        auto p3 = new Person{"berit", 32};
        delete p3;
    }

    {
        cout << "------\n";
        auto const N       = 5U;
        auto       persons = vector<Person>{};
        persons.reserve(N);
        for (auto k = 1U; k <= N; ++k)
            persons.emplace_back("per-" + to_string(k), 10 * k);
    }

    {
        cout << "------\n";
        auto const N       = 5U;
        auto       persons = vector<Person*>{};
        persons.reserve(N);
        for (auto k = 1U; k <= N; ++k)
            persons.push_back(new Person{"nils-" + to_string(k), 10 * k});
        for (auto ptr : persons) delete ptr;
    }

    cout << "# persons: " << Person::getCount() << endl;
    return 0;
}
