#pragma once

#include "Rect.hxx"


class Square : public Rect {
    using super = Rect;
public:
    Square(int side)
            : super(side, side) {
        cout << "Square{" << side << "} @ " << this << endl;
    }

    ~Square() {
        cout << "~Square() " << this << endl;
    }
};
