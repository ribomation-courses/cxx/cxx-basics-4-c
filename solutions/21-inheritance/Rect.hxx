#pragma once

#include "Shape.hxx"

using namespace std;


class Rect : public Shape {
    using super = Shape;
    int width, height;

public:
    Rect(int width, int height) : super{"rect"}, width{width}, height{height} {
        cout << "Rect{" << width << ", " << height << "} @ " << this << endl;
    }

    ~Rect() {
        cout << "~Rect() @ " << this << endl;
    }

    double area() const override {
        return width * height;
    }

    string toString() const override {
        ostringstream buf;
        buf << super::toString() << "{" << width << ", " << height << "}";
        return buf.str();
    }
};

