#include <iostream>
#include <string>
#include <random>
#include <vector>
#include <memory>
#include "Shape.hxx"
#include "Rect.hxx"
#include "Circle.hxx"
#include "Triangle.hxx"
#include "Square.hxx"
using namespace std;


Shape* mkShape() {
    static random_device          r;
    uniform_int_distribution<int> nextShape{1, 4};
    uniform_int_distribution<int> nextValue{1, 10};

    cout << "****\n";
    switch (nextShape(r)) {
        case 1:
            return new Rect{nextValue(r), nextValue(r)};
        case 2:
            return new Circle{nextValue(r)};
        case 3:
            return new Triangle{nextValue(r), nextValue(r)};
        case 4:
            return new Square{nextValue(r)};
    }

    throw domain_error("WTF: this should not happen");
}

void print(const Shape& s) {
    cout << s << endl;
}

void usingRawPointers(unsigned N) {
    cout << "---- raw ptr ---\n";
    auto shapes = vector<Shape*>{};
    while (N-- > 0) { shapes.push_back(mkShape()); }

    cout << "----print-----------------\n";
    for (auto s : shapes) print(*s);

    cout << "----delete-----------------\n";
    while (!shapes.empty()) {
        auto s = shapes.back();
        shapes.pop_back();
        cout << "****\n";
        delete s;
    }
}


void usingSmartPointers(unsigned N) {
    cout << "\n\n---- smart ptr ---\n";
    auto shapes = vector<shared_ptr<Shape>>{};
    shapes.reserve(N);
    while (N-- > 0) { shapes.push_back(shared_ptr<Shape>{mkShape()}); }

    cout << "----print-----------------\n";
    for (auto s : shapes) print(*s);

    cout << "----delete-----------------\n";
}


int main(int numArgs, char* args[]) {
    auto N = (numArgs <= 1) ? 5U : stoi(args[1]);
    usingRawPointers(N);
    usingSmartPointers(N);
    return 0;
}
