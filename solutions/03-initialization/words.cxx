#include <iostream>
#include <string>
#include <set>
using namespace std;

int main() {
    set<string> words = {
            "C++", "is", "indeed","a", "cool", "language", "!!",
            "is", "C++", "cool",
    };
    words.insert("tjabba");

    string    arr[] = {"aa", "BB", "!!", "!!!", "zzzz", "C++", "C++"};
    const int N     = sizeof(arr) / sizeof(arr[0]);
    words.insert(arr, arr + N);

    for (string const& w : words) cout << w << endl;
    return 0;
}
