#include <iostream>
#include <fstream>
#include <vector>
#include <string>
using namespace std;
using namespace std::literals;

using Strings = vector<string>;

auto load(istream&) -> Strings;
auto totalLength(const Strings&) -> unsigned;
auto operator <<(ostream&, const Strings&) -> ostream&;

int main(int argc, char** argv) {
    auto filename = (argc == 1) ? "../words.cxx"s : argv[1];
    auto file     = ifstream{filename};
    if (!file) throw invalid_argument{"cannot open "s + filename};

    auto verbose = true;
    auto words   = load(file);
  //  if (verbose) cout << "[1] words: " << words << endl;

    auto totLen = totalLength(words);
    cout << "[2] total: " << totLen << endl;

    words.insert(words.begin(), "tjolla hopp"s);
    //if (verbose) cout << "[3] words: " << words << endl;
    operator <<(operator <<(operator <<(cout, "[3] words: "), words), "\n");

    // struct Ostream cout={....};
    // struct Ostream* myprint(struct Ostream* os, char* msg) {.... return os;}
    // myprint(myprint(&cout, "tjabba"), "habba")

    cout << "[4] words: ";
    while (!words.empty()) {
        auto w = words.back();
        //cout << w << " ";
        words.pop_back();
    }
    cout << endl;

    return 0;
}

auto load(istream& in) -> Strings {
    auto        words = Strings{};
    for (auto word = ""s; in >> word;) words.push_back(word);
    return words;
}

unsigned totalLength(const Strings& words) {
    auto len = 0U;
    for (auto const& w : words) len += w.size();
    return len;
}

auto operator <<(ostream& os, const Strings& vec) -> ostream& {
    os << "[";
    bool first = true;
    for (auto const& s : vec) {
        if (first) first = false; else os << "; ";
        os << s;
    }
    os << "]";   // cout << "]"
    return os;
}