#include <iostream>
#include <functional>
using namespace std;

void repeat(unsigned n, function<void (unsigned)> stmt) {
    for (auto k=1U; k <= n; ++k) stmt(k);
}

int main() {

    repeat(5, [](auto num){ cout << num << ") C++ is cool\n"; });

    return 0;
}
