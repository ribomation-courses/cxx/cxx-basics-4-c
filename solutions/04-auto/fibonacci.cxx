#include <iostream>
#include <tuple>
#include <map>
using namespace std;

auto fib(unsigned n) {
    if (n == 0) return 0U;
    if (n == 1) return 1U;
    return fib(n - 2) + fib(n - 1);
}

auto compute(unsigned n) {
    return make_tuple(n, fib(n));
}

auto create(unsigned n)  {
    auto result = map<unsigned, unsigned>{};
    for (auto k = 1U; k <= n; ++k) {
        auto[arg, fib] = compute(k);
        result[arg] = fib;   //similar: result.insert( make_pair(arg,fib) );
    }
    return result;
}

int main() {
    auto N = 20;
    for (auto [arg, fib] : create(N))
        cout << "fib(" << arg << ") = " << fib << endl;
    return 0;
}
