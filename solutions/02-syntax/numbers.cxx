#include <iostream>
#include <limits>
using namespace std;

int main() {
    int num = 0;
    int cnt = 0;
    int sum = 0;
    int minval = numeric_limits<short>::max();
    int maxval = numeric_limits<int>::min();
    do {
        cout << "Enter number: ";
        cin >> num;
        if (num == 0 || !cin) break;

        ++cnt;
        sum += num;
        minval = ::min(minval, num);
        maxval = ::max(maxval, num);
    } while (num != 0);

    cout << "------\n";
    cerr << "# values: " << cnt << endl;
    cerr << "average : " << static_cast<double>(sum) / static_cast<double>(cnt) << endl;
    cerr << "min/max : " << minval << " / " << maxval << endl;

    return 0;
}
