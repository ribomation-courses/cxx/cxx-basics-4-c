cmake_minimum_required(VERSION 3.14)
project(08_strings)

set(CMAKE_CXX_STANDARD 17)

add_executable(string-utils
        string-utils.hxx
        string-utils-test.cxx)

