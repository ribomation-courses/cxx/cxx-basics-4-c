# C++ for C Programmers, 4 days

Welcome to this course. The syllabus can be found at
[cxx/cxx-basics-4-c](https://www.ribomation.se/courses/cxx/cxx-basics-4-c.html)

Here you will find
* Installation instructions
* Solutions to the programming exercises
* Sources to the demo programs

_N.B._ Solutions and Demos programs will be pushed during the course.

This course in C++ programming is aimed at experienced C programmers, whom as quickly as possible want to start programming i C++. We consistenty refer to C and illustrate similarities and point out differences.

# Installation Instructions

To perform the programming exercises, you need:
* A modern C++ compiler, supporting `C++ 17`, to compile your code
* A C++ IDE, to write your code
* A GIT client, to get the solutions from this repo

## Recommended Setup

Although, there are many ways to write and compile C++ programs both on Windows,
Linux and Mac, our experience of giving many courses on various levels; is that
using Ubuntu Linux provides the least amount of surprises and distracting
technical struggles.

For this course, we do recommend the following setup:
* Ubuntu Linux, version 19.04 (_or later_)
* GCC C++ compiler, version 9
* JetBrains CLion IDE (_30-days trial_)

# Ubuntu Linux @ VirtualBox

Install VirtualBox, create a virtual machine for Ubuntu and download/install Ubuntu to it.

1. Install VirtualBox `version 6` (VBox)<br/>
    <https://www.virtualbox.org/wiki/Downloads>
1. Create a new virtual machine (VM) i VBox, for Ubuntu Linux<br/>
    <https://www.virtualbox.org/manual/ch03.html>
1. Download an ISO file for the latest version of Ubuntu Desktop 64-bit, `version 19.04` (or later)<br/>
    <http://www.ubuntu.com/download/desktop>
1. Choose as much RAM for the VM as you can, still within the "green" region.
1. Choose as much video memory you can
1. Create an auto-expanding virtual hard-drive of (at least) `50GB`
1. Mount the ISO file in the virtual CD drive of your VM
1. Start the VM and run the Ubuntu installation program.
    Ensure you install to the (virtual) hard-drive.
1. Set a username and password when asked to and write them down so you remember them.<br/>
   Also, go for auto-logon.

## Inside Ubuntu: _Install the following_

1. Install the VBox `guest additions`<br/>
    <https://www.virtualbox.org/manual/ch04.html><br/>
    Don't forget to reboot, after it's done.
1. Install the GCC compiler and build tools (_see below_)
1. Install GIT and clone this repo (_see below_)
1. Install Clion (_see below_)

### GCC C++ Compiler and Tools

Within a Ubuntu terminal window type the following command to install the
compiler and other tools.

    sudo apt install g++-9 cmake make gdb valgrind tree

_N.B._ when you run a `sudo` command it prompts you for the password, you use
to logon to Ubuntu.

Change the default GCC version

    sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-9 90 --slave /usr/bin/g++ g++ /usr/bin/g++-9 --slave /usr/bin/gcov gcov /usr/bin/gcov-9

Verify that GCC version 9, now is the default

    gcc --version
    g++ --version

### GIT Client

Install GIT and clone this repo

    sudo apt git

Create a base directory for this course and a dedicated sub-folder for your own solutions

    mkdir -p ~/cxx-course/my-solutions
    cd ~/cxx-course
    git clone https://gitlab.com/ribomation-courses/cxx/cxx-basics-4-c.git gitlab

During the course, solutions will be push:ed to this repo and you can get these by a git pull operation

    cd ~/cxx-course/gitlab
    git pull

### JetBrains CLion

CLion is, in my personal opinion, the best IDE for C/C++ development.
For this course, I do recommend to try it out. 

You can easily install it from the application app of Ubuntu
(_icon to the left with a big A_). Type "`clion`" in the _search bar_ and 
install it.

After installation, you can find it in the programs menu (_left bottom with 9 dots in a square_). The IDE is really easy to work with, but please check out the 
docs, tutorials and "Quick Tour" video at
* https://www.jetbrains.com/clion/learning-center/


# Ubuntu Linux (WSL) @ Windows 10

One of the biggest news with Windows 10, is that it provides the _Windows Subsystem for
Linux_ (WSL) plugin. Just open _Microsoft Store_ and search for "Ubuntu". Then choose
Ubuntu 18.04 and install it. It's as simple as that.

Our favorite C++ IDE is CLion and it works very nicely with WSL; i.e. you install
CLion on Windows and uses the compiler resources within WSL. Just follow the 
instructions at
* [How to Use WSL Development Environment in CLion](https://www.jetbrains.com/help/clion/how-to-use-wsl-development-environment-in-clion.html)
* [Using WSL toolchains in CLion on Windows (YouTube)](https://youtu.be/xnwoCuHeHuY)

## GCC Version
Ubuntu verison 18, do not have GCC version 9 _by default_.
Which means that you cannot compile a few demo programs of the course.
For the majority of the programming exercises and demo programs, you can
stick with GCC 8.


# Mac User
If you have a Mac laptop, install a Modern C++ compiler supporting C++ 17
and install JetBrains CLion.

# Linux User
If you're running a different Linux desktop OS, such as Fedora or similar; use its
package manager to install a compiler supporting C++ 17 and then install CLion.

# Usage of this GIT Repo
Ensure you have a [GIT client](https://git-scm.com/downloads) installed and clone
this repo.

    mkdir -p ~/cxx-course/my-solutions
    cd ~/cxx-course
    git clone https://gitlab.com/ribomation-courses/cxx/cxx-basics-4-c.git gitlab
    cd gitlab && ls -lhFA


During the course, solutions will be push:ed to this repo and you can get these by
a git pull operation

    cd ~/cxx-course/gitlab
    git pull

# Build Solution/Demo Programs
The solutions and demo programs are all using CMake as the build tool. CMake is a cross
platform generator tool that can generate makefiles and other build tool files. It is also
the project descriptor for JetBrains CLion, which is my IDE of choice for C/C++ development.

You don't have to use CLion in order to compile and run the sources. What you do need have; 
are cmake, make and gcc/g++ all installed. When you want to build a solution or demo; 
First change into its project directory, then run the  commands below.

    cd path/to/some/solutions/dir
    mkdir -p build && cd build
    cmake -G  ..
    cmake --build .

The executable is now in the `./build/` directory.


# Using Windows for the Course
You can write your solutions for this course using
*MS Visual Studio* running on Windows. However, the majority of your customers are targeting *NIX systems, which is why we primary recommend using an environment such as Ubuntu Linux.

Any way, if you already feel comfortable using MS Visual Studio, then use it for this course.
* [MS Visual Studio, overview & download](https://www.visualstudio.com/vs/)

# Interesting Links
* [CppCon 2018: Bjarne Stroustrup “Concepts: The Future of Generic Programming (the future is here)”](https://youtu.be/HddFGPTAmtU)
* [CppCon 2015: Eric Niebler "Ranges for the Standard Library"](https://youtu.be/mFUXNMfaciE)
* [CppCon 2015: Bjarne Stroustrup “Writing Good C++14”
](https://youtu.be/1OEu9C51K2A)
* [C++ Core Guidelines](http://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines)
* [CppReference](https://en.cppreference.com/w/)


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>



